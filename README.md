##### M3-Programació.  Uf3.-Fonaments de gestió de fitxers #####
 Documentació de Gestió de Fitxers 
***
### EXERCICIS: **Fitxers binaris amb accés seqüencial** ###
Aquest  exercici es basa en  la  gestió  de  fitxers  binaris  amb  accés  seqüencial. 
Aquests són els apartats amb els que compta l'exercici:

![imagen menú](menu.png)


<br>

Introducció
-
Constants correctament definides
    
    typedef struct {
        char tipus[MAX_TIPUS+1];
        char nom[MAX_NOM+1]; -> és el camp clau
        char sexe; -> M - Mascle / F - Femella
        bool perilldExtincio;
        int edat;
        double pes;
    }Animals;


<br><br>

<a name="top"></a>
Menú
-
0. Sortir
1. [Alta](#alta)
2. [Baixa](#baixa)
3. [Consulta](#consulta)
4. [Modificació](#modificacio)
5. [Consultar Esborrats](#cons_esborrats)
6. [Esborrar Fitxer](#esbborrar_fitx)
7. [Compactar Fitxers](#compact_fitx)
8. [Accés Directe](#acc_directeinum_registres)
9. [Número de registres](#acc_directeinum_registres)
10. [Informe](#informe)


<a name="alta"></a>
<br>
## 1.Alta
Aquesta opció de gestió tracta de:
- **Obrir el fitxer** 

En mode **_ab_** per a poder guardar i afegir dades i que no es machaquin.<br>
Aquest mode deixa l’apuntador després del darrer registre preparat per afegir dades al fitxer. A més, en el cas que el fitxer no existeixi, que es crei automàticament.

> `Animals a1;`   -> Estructura i variable
>
>`FILE *f1;`     -> Variable de tipus fitxer
>
>`int n;  `       -> Per a comptar els registros
>
> `f1=fopen(nomFitxer,"ab");`     -> fopen() Per a obrir un fitxer

<br>

__Robustesa__: Control de errors.<br>
 Si  per algun motiu retorna  un valor NULL, significarà que no s’ha pogut obrir correctament el fitxer.

    if( f1 == NULL ) {
     return -1; // Error en obrir el fitxer
    }

Un cop hagi superat el control d'errors, es podrà operar en el fitxer correctament.

<br>


* **Entrar dades** 

Ja amb les dades dels camps de tipus Animals entrades s'han de salvar en el fitxer extern, és a dir, al dispositiu d’emmagatzematge massiu amb el següent codi:

    n=fwrite(&a1,sizeof(Animals),1,f1);

<br>

__Robustesa__: Control de errors.<br>
La  funció fwrite retorna  el  nombre  de  registres (n) que s’han pogut escriure correctament.
Per tant, sempre hauria de coincidir amb el tercer paràmetre.
En el cas de que no hi coincideixin significarà que hi ha hagut algun error .

    if(n==0) {
        return -2; // ERROR ESCRIPTURA
    }

<br>

* **Tancar Fitxer**

Finalment, després d’haver enregistrat tots els registres s'ha de tancar el fitxer:

    fclose(f1);

<br>

 [Pujar a índex](#top)


<br>


<a name="baixa"></a>
<br>
## 2. Baixa
Per fer la baixa primer s'ha de buscar el fitxer del registre que es vol i després desplaçar l’apuntador del fitxer uns bytes endarrere (un registre enrrere) amb la funció fseek.

Aquesta opció de gestió tracta de:
- **Obrir el fitxer** <br>

Per fer la baixa hem d’obrir el fitxer en mode **“rb+”**.  Lectura/Escriptura en binari. 

    f1 = fopen(nomFitxer,"rb+");


<br>

S'ha d'afegir un camp de tipus char al fitxer intern que ens permetrà marcar el registre com esborrat assignant un ‘*’ als registres que vulguem esborrar. 

Aquesta es com quedda la estructura del fitxer intern:

    typedef struct {
        char tipus[MAX_TIPUS+1];
        char nom[MAX_NOM+1]; -> és el camp clau
        char sexe; -> M - Mascle / F - Femella
        bool perilldExtincio;
        int edat;
        double pes;
      char marcaBorrat; -> Escriure asterísc 
    }Animals;


D'aquesta manera a l'hora de fer consultes, baixes o modificacions comprovarem aquest camp i si conté un
asterisc, poder saltar-nos el registre sense tractar aquell registre, ja que "no existeix".

<br>

Cal demanar el valor del camp clau per buscar el registre que es vol esborrar, en el nostre cas el camp es el NOM.
    
    char nomB[MAX_NOM];   -> variable on guardarem el   nom que introdueix l'usuari

    printf("\nIntrodueix el nom (ID) per a buscar el registre: ");
    scanf("%"xstr(MAX_NOM)"[^\n]",nomB); BB;

<br>

__Robustesa__: Control de errors.<br>

La lectura de registres
s’hauria de repetir fins a trobar el registre que busquem.

    while(!feof(f1)){
            n=fread(&a1,sizeof(Animals),1,f1);
            if(!feof(f1)){
            if(n==0) return -1;

<br>

Un cop trobat, la baixa consistiria en assignar un asterisc al camp a1.marcaBorrat.

> if(a1.marcaBorrat!='*' && strcmp(a1.nom,nomB)==0 ){   -> retorna 0 si es cert

Quan fem una lectura amb fread() l’apuntador del fitxer passa al següent registre.
Per tant quan hem trobat el registre que es vol esborrar, l’apuntador està sobre el següent registre.
El que s'ha de fer es enderrareir l'apuntador una posició, a partir desde la seva posició actual (SEEK_CUR).
    
    if(fseek(f1,-(long)sizeof(Animals),SEEK_CUR))


Finalment ens quedaria tancar el fitxer.
   
    fclose(f1);

<br>

 [Pujar a índex](#top)


<br>


<a name="consulta"></a>
<br>
## 3. Consulta
Aquesta opció de gestió tracta de:
 * __Obrir el fitxer.__ En mode **“rb”**,
    
        f1=fopen(nomFitxer,"rb");
 
 * Recórrer el fitxer fent lectures

        while(!feof(f1)){
        n=fread(&a1,sizeof(Animals),1,f1); 
        if(!feof(f1)){
            if(n==0) {
                printf("Error de lectura");
                return -3;
            }

 * Mostrant els registres que no estan marcats per esborrar.
        
         if(a1.marcaBorrat!='*') mostrarDadesAnim(a1);


 * Tancar el fitxer
    
         fclose(f1);

<br>

 [Pujar a índex](#top)


<br>


<a name="modificacio"></a>
<br>
## 4. Modificació
Com que l'estratègia es la mateixa que la de la baixa i el codi és molt similar hem juntat les dues funcions i únicament a partir d'un condicional quan es trobi el registre que es busca s'assignaràn les noves dades.

<br>

Bool de seguirBaixaoModi() per a fer confirmar a l'usuari si realment vol modificar o donar de baixa el registre determinat. 

    if(!seguirBaixaoModi(modificar)){return -4;}
    if(seguirBaixaoModi(modificar)){


També ens caldrà moure l’apuntador un registre endarrere.
        
    if(fseek(f1,-(long)sizeof(Animals),SEEK_CUR)) {return -2;} 
    
<br>

bool __modificar__:
Si no es vol modificar, farà la baixa i afegirà un asterísc:

    if(!modificar){
        a1.marcaBorrat='*'; 
        n=fwrite(&a1,sizeof(Animals),1,f1); // en bytes
    
Si no, modificarà

        }else --> 
        if(modificar){
            entrarAnimal(&a1,modificar);
            n=fwrite(&a1,sizeof(Animals),1,f1); // en bytes
        }

<br>

 [Pujar a índex](#top)


<br>


<a name="cons_esborrats"></a>
<br>
## 5. Consultar Esborrats
Es el mateix codi que l’opció consulta però en comptes de mostrar els que no tenen un asterisc al camp
marcaBorrat, caldria mostrar els que sí que tenen un asterisc.

    if(!esborrats){
            if(a1.marcaBorrat!='*') mostrarDadesAnim(a1);
           }else{
            if(a1.marcaBorrat=='*') mostrarDadesAnim(a1);
           }

<br>

 [Pujar a índex](#top)


<br>


<a name="esbborrar_fitx"></a>
<br>
## 6. Esborrar Fitxer
Aquesta opció ens es per esborrar el fitxer de dades que hi ha en el dispositiu extern.

Cal incloure la llibreria <unistd.h>.

    int esborrarFitxer(char nomFitxer[]){
        return unlink(nomFitxer);
    }


<br>

 [Pujar a índex](#top)


<br>


<a name="compact_fitx"></a>
<br>
## 7. Compactar Fitxers
Aquesta opció consisteix en esborrar físicament al fitxer extern els registres marcats per esborrar.

Aquesta opció de gestió tracta de:
- **Obrir el fitxer**. Mode **“rb”**.

        FILE *origen, *desti;
        origen=fopen(nomFitxer,"rb");
        if(origen==NULL) return -1;

- Obrir un fitxer auxiliar o temporal per a escriure els registres que no estan marcats per
esborrar, *“tmp.dat” (“wb”)*.
       
        desti=fopen("tmp.dat","wb");
        if(desti==NULL) return -1;

* Llegir tots els registres del fitxer "animals.dat”

        while(!feof(origen)){
            n=fread(&animal,sizeof(Animals),1,origen);
            if(!feof(origen)){
                if(n==0) {
                    return -3;
                }

* Escriure els registres al fitxer temporal “tmp.dat" sempre i quan no estiguin marcats per esborrar.

        if(animal.marcaBorrat!='*'){
            n=fwrite(&animal,sizeof(Animals),1,desti);
        }

* Un cop copiats els registres, tancar els dos fitxers.
       
        fclose(origen);
        fclose(desti);

* Esborrar el fitxer "animals.dat”.
       
        esborrarFitxer(nomFitxer);

* I per últim renombrar el fitxer auxiliar “tmp.dat" a “animals.dat”.
        
        if(rename("tmp.dat",NOM_FITXER)==-1){
            return -5;
        }

<br>

__Robustesa__: Aquest control d' errors retorna -1 si no ha pogut renombrar el fitxer.


<br>

 [Pujar a índex](#top)


<br>


<a name="acc_directe"></a>
<br>
## 8 i 9. Accés Directe i Número de registres

Primer haurem de saber el nombre de registres:

        long nReg;
        
        fseek(f1,(long)0L,SEEK_END);
        nReg=ftell(f1);
        nReg=nReg/sizeof(Animals);

<br>

Preguntar a quin registre es vols accedir:
    
    do{
        printf("\nA quin registre vols accedir? ");
        scanf("%i",&pos);BB;
    }while(pos>total || pos<0);

<br>

Com tots els registres tenien longitud fixa, amb la funció fseek es podria accedir directament per posició a un determinat registre.

        n = pos-1; -> El primer registre és troba al byte 0
        fseek(f1, (long) (n*sizeof(Animals)),SEEK_SET);


<br>

 [Pujar a índex](#top)


<br>


<a name="informe"></a>
<br>
## 10. Informe

Aquesta opció crearà un fitxer de text “informe.html” i es mostrarà en una pàgina web al anvegador on apareixerà una taula amb els animals que hi ha donats d’alta. 

<br>

![imagen informe](informe.png)

<br>


        if((fitxerHTML=fopen("“informe.html","w"))==NULL){
            printf("Error obrir fitxer");
            control=-1;
        }


* Concatenar cadenes ***(strcat)***:

        char total[MAX_TOTAL+1];
        total[0]='\0';
            strcat(total,"\n\t\t\t<tr>");
            strcat(total,"\n\t\t\t\t<td>");
            strcat(total,a1.nom);
            strcat(total,"</td>");

__total__ té la cadena que tenia més la cadena nova.

<br>

* Conversió d’enter a cadena ***(sprintf)***:

      sprintf(tmp,"%.2lf",a1.pes);


<br>

 [Pujar a índex](#top)
